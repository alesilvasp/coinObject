const main = document.getElementById('main')
const container = document.createElement('div');
container.id = 'container';
container.classList.add('main_container')
main.appendChild(container)
const coinsText = document.createElement('span')

const coin = {
    state: 0,

    flip: function () {
        this.state = Math.floor(Math.random() * 2);
        return this.state
    },

    toString: function () {
        let headOrTails = coin.flip();
        if (headOrTails === 0) {
            return 'Heads'
        }
        return 'Tails'
    },

    toHTML: function () {
        const image = document.createElement("img");
        image.classList.add('coins')
        let string = this.toString();
        if (string === 'Heads') {
            image.src = './img/cara.png';
            image.alt = 'heads'
        } else {
            image.src = './img/coroa.png';
            image.alt = 'tails'

        }
        return image;
    },
};

let numberOfFlips = 20;

function display20Flips(flips) {
    const results = [];
    for (let i = 0; i < flips; i++) {
        results.push(coin.toString())
    }
    let resultsString = results.join(', ')
    coinsText.innerText = resultsString;
    container.appendChild(coinsText)
    return results
}

function display20Images(flips) {
    const results = [];
    for (let i = 0; i < flips; i++) {
        results.push(coin.toHTML())
        container.appendChild(results[i])
    }
    return results
}

display20Images(numberOfFlips);
display20Flips(numberOfFlips);